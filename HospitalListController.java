
//package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
//import model.Hospital;
//import view.HospitalListJavaFXView;

public class HospitalListController {
	@FXML
	private TextField searchKey;
	@FXML
	private TableView<Hospital> hospitalTable;
	@FXML
	private TableColumn<Hospital, String> nameColumn;
	@FXML
	private TableColumn<Hospital, String> streetAddressColumn;
	@FXML
	private TableColumn<Hospital, String> cityColumn;
	@FXML
	private TableColumn<Hospital, String> stateColumn;
	@FXML
	private TableColumn<Hospital, String> zipColumn;
	@FXML
	private TableColumn<Hospital, String> latitudeColumn;
	@FXML
	private TableColumn<Hospital, String> longitudeColumn;
	@FXML
	private TableColumn<Hospital, String> phoneNoColumn;
	@FXML
	private TableColumn<Hospital, String> photoColumn;

	private HospitalListJavaFXView hospitalApp;

@FXML
private void initialize() {
	nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
	streetAddressColumn.setCellValueFactory(cellData -> cellData.getValue().streetAddressProperty());
	cityColumn.setCellValueFactory(cellData -> cellData.getValue().cityProperty());
	stateColumn.setCellValueFactory(cellData -> cellData.getValue().stateProperty());
	zipColumn.setCellValueFactory(cellData -> cellData.getValue().zipProperty());
	latitudeColumn.setCellValueFactory(cellData -> cellData.getValue().latitudeProperty());
	longitudeColumn.setCellValueFactory(cellData -> cellData.getValue().longitudeProperty());
	phoneNoColumn.setCellValueFactory(cellData -> cellData.getValue().phoneNoProperty());
	photoColumn.setCellValueFactory(cellData -> cellData.getValue().photoProperty());

	}

public void setHospitalApp(HospitalListJavaFXView hospitalApp) {
	this.hospitalApp = hospitalApp;
	hospitalTable.setItems(hospitalApp.getHospitalData());

	}

public void handleEnterPressed(KeyEvent event) {
	String latitude = "";
	String longitude = "";
	String key = "";
	boolean coordinate = true;
	Hospital hospitalKey;
	ObservableList<Hospital> searchHospitalResultTable = FXCollections.observableArrayList();
	if (event.getCode() == KeyCode.ENTER) {
	key = searchKey.getText();
	if (coordinate) {
	latitude = key.split(",")[0];
	longitude = key.split(",")[1];
	System.out.println(latitude + " " + longitude);
	hospitalKey = new Hospital(latitude, longitude);
	if (hospitalApp.getHospitalBSTree().contains(hospitalKey)) {
	searchHospitalResultTable.add(hospitalApp.getHospitalBSTree().get(hospitalKey));
	hospitalApp.setHospitalData(searchHospitalResultTable);
	hospitalTable.setItems(hospitalApp.getHospitalData());
	} else {
	searchHospitalResultTable.clear();
	hospitalApp.setHospitalData(searchHospitalResultTable);
	hospitalTable.setItems(hospitalApp.getHospitalData());

	}
	}
	}
	}

}
