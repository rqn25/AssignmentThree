
//package sample;

import javafx.fxml.FXML;
//import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
//import model.Person;

public class PersonOverviewController {
	@FXML
	private TableView<Person> personTable;
	@FXML
	private TableColumn<Person, String> firstNameColumn;
	@FXML
	private TableColumn<Person, String> lastNameColumn;

	private Main mainApp;

public PersonOverviewController() {

	}

@FXML
private void initialize() {
	firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
	lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());

	}

public void setMainApp(Main mainApp) {
	this.mainApp = mainApp;
	personTable.setItems(mainApp.getPersonData());

	}

}
