
public interface UnboundedStackInterface<T> extends StackInterface<Object> {
	void push(T element);
	// Places element at the top of this stack.

}
