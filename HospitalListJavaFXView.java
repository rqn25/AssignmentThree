
//package view;

import java.io.IOException;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class HospitalListJavaFXView {
	private static Stage hospitalListStage = new Stage();
	private String name;
	private String streetAddress;
	private String city;
	private String state;
	private String zip;
	private String latitude;
	private String longitude;
	private String phoneNo;
	private String photo;
	private ObservableList<Hospital> hospitalData = FXCollections.observableArrayList();

public BinarySearchTree<Hospital> getHospitalBSTree() {
	return hospitalBSTree;

	}

	private BinarySearchTree<Hospital> hospitalBSTree;
	private BorderPane rootLayout;

public HospitalListJavaFXView() throws IOException {
	loadHospital();
	initRootLayout();
	showHospitalView();
	hospitalListStage.setTitle("Hospital List Page");

	}

private void initRootLayout() {
	try {
	FXMLLoader loader = new FXMLLoader();
	loader.setLocation(HospitalListJavaFXView.class.getResource("RootLayout.fxml"));
	rootLayout = loader.load();
	Scene scene = new Scene(rootLayout);
	hospitalListStage.setScene(scene);
	hospitalListStage.show();
	} catch (IOException e) {
	e.printStackTrace();

	}
	}

public void loadHospital() {
	hospitalBSTree = new BinarySearchTree<Hospital>();
	List hospitalList = null;
	try {
	hospitalList = ReadExcelFile.excelReader("HospitalList.xls");
	} catch (Exception e) {
	System.err.println("Problem reading HospitalList.xls file");
	e.printStackTrace();
	}
	for (int i = 0; i < hospitalList.size(); i++) {
	List record = (List) hospitalList.get(i);
	name = String.valueOf(record.get(0));
	streetAddress = String.valueOf(record.get(1));
	city = String.valueOf(record.get(2));
	state = String.valueOf(record.get(3));
	zip = String.valueOf(record.get(4));
	latitude = String.valueOf(record.get(5));
	longitude = String.valueOf(record.get(6));
	phoneNo = String.valueOf(record.get(7));
	photo = String.valueOf(record.get(8));

	Hospital hospital = new Hospital(name, streetAddress, city, state, zip, latitude, longitude, phoneNo, photo);
	hospitalBSTree.add(hospital);
	hospitalData.add(hospital);

	}
	}

public void showHospitalView() {
	try {
	FXMLLoader loader = new FXMLLoader();
	loader.setLocation(HospitalListJavaFXView.class.getResource("HospitalListJavaFX.fxml"));
	AnchorPane hospitalOverview = loader.load();
	rootLayout.setCenter(hospitalOverview);
	HospitalListController controller = loader.getController();
	controller.setHospitalApp(this);
	} catch (IOException e) {
	e.printStackTrace();

	}
	}

public ObservableList<Hospital> getHospitalData() {
	return hospitalData;

	}

public void setHospitalData(ObservableList<Hospital> searchHospitalResultTable) {

	}

}
