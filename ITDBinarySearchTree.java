
import java.util.Scanner;

public class ITDBinarySearchTree {

public static void main(String[] args) {
	Scanner conIn = new Scanner(System.in);
	String skip;
	boolean keepGoing;
	int operation;
	int order;
	String testName;
	BinarySearchTree<String> tree = new BinarySearchTree<>();
	String element;
	String target;
	int treeSize;

	System.out.println("What is the name of this test?");
	testName = conIn.nextLine();
	System.out.println("\nThis is test " + testName + " ");

	keepGoing = true;
	while (keepGoing) {
	System.out.println("\nChoose an operation:");
	System.out.println("1: isEmpty");
	System.out.println("2: size");
	System.out.println("3: size2");
	System.out.println("4: contains (string)");
	System.out.println("5: remove (string)");
	System.out.println("6: get (string)");
	System.out.println("7: add (string)");
	System.out.println("8: print (traversal) order)");
	System.out.println("9: stop testing\n");
	System.out.print("Enter choice: ");

	if (conIn.hasNextInt())
	operation = conIn.nextInt();
	else {
	System.out.println("Error: you must enter an integer.");
	System.out.println("Terminating test.");
	return;

	}

	skip = conIn.nextLine();

	switch (operation) {
	case 1:
	System.out.println("isEmpty returns " + tree.isEmpty());
	break;

	case 2:
	System.out.println("size returns " + tree.size());
	break;

	case 3:
	try {
	System.out.println("size2 returns " + tree.size2());
	} catch (UnderflowException e) {
	System.out.println("Tree is empty.");
	e.printStackTrace();
	}
	break;

	case 4:
	System.out.print("Enter string to search for: ");
	target = conIn.nextLine();
	System.out.println("contains (" + target + ") returns " + tree.contains(target));
	break;

	case 5:
	System.out.print("Enter string to remove: ");
	target = conIn.nextLine();
	System.out.println("remove (" + target + ") returns " + tree.remove(target));
	break;

	case 6:
	System.out.print("Enter string to get: ");
	target = conIn.nextLine();
	System.out.println("get (" + target + ") returns " + tree.get(target));
	break;

	case 7:
	System.out.print("Enter string to add: ");
	element = conIn.nextLine();
	tree.add(element);
	break;

	case 8:
	System.out.println("Choose a traversal order:");
	System.out.println("1: Preorder");
	System.out.println("2: Inorder");
	System.out.println("3: Postorder");
	if (conIn.hasNextInt())
	order = conIn.nextInt();
	else {
	System.out.println("Error: you must enter an integer.");
	System.out.println("Terminating test.");
	return;

	}

	skip = conIn.nextLine();

	switch (order) {
	case 1:
	treeSize = tree.reset(BinarySearchTree.PREORDER);
	System.out.println("The tree in Preorder is:");
	for (int count = 1; count <= treeSize; count++) {
	element = (String) tree.getNext(BinarySearchTree.PREORDER);
	System.out.println(element);
	}
	break;

	case 2:
	treeSize = tree.reset(BinarySearchTree.INORDER);
	System.out.println("The tree in Inorder is:");
	for (int count = 1; count <= treeSize; count++) {
	element = (String) tree.getNext(BinarySearchTree.INORDER);
	System.out.println(element);
	}
	break;

	case 3:
	treeSize = tree.reset(BinarySearchTree.POSTORDER);
	System.out.println("The tree in Postorder is:");
	for (int count = 1; count <= treeSize; count++) {
	element = (String) tree.getNext(BinarySearchTree.POSTORDER);
	System.out.println(element);
	}
	break;

	}
	}
	}
	}

}
